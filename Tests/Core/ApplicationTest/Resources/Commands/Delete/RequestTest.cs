﻿using Monosoft.Common.Exceptions;
using Monosoft.Order.Application.Resources.Order.Commands.Delete;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationTest.Resources.Commands.Delete
{
    [TestFixture]
    public class RequestTest
    {
        [Test]
        public void Id_Guid_Emty()
        {
            Request res = new Request();
            res.Id = Guid.Empty;
           
            Assert.Throws<ValidationException>(() => res.Validate());
        }
    }
}
