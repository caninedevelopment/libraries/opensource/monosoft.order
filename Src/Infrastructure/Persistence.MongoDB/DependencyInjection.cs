namespace Monosoft.Order.Persistence.MongoDB
{
    using Domain.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Monosoft.Order.Persistence.MongoDB.Repositories;

    public static class DependencyInjection
    {
        public static IServiceCollection AddMongoDBPersistenceProvider(this IServiceCollection services, string dbServerHost, string dbUser, string dbPassword, int dbServerPort, string dbName)
        {
            var context = new DbContext(dbUser, dbPassword, dbServerHost, dbServerPort, dbName);
            services.AddSingleton(context);

            services.AddSingleton<IOrderRepository, OrderRepository>();

            return services;
        }
    }
}