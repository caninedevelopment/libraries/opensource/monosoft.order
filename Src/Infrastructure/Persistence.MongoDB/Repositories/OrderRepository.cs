﻿namespace Monosoft.Order.Persistence.MongoDB.Repositories
{
    using Domain.Entities.Order;
    using Domain.Interfaces;
    using global::MongoDB.Driver;
    using System;
    using System.Collections.Generic;

    public class OrderRepository : IOrderRepository
    {
        private readonly DbContext context;

        public OrderRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "token";

        private IMongoCollection<Order> collection
        {
            get { return this.context.db.GetCollection<Order>(collectionName); }
        }

        public void Delete(Guid id)
        {
            collection.DeleteOne(x => x.Id == id);
        }

        public List<Order> GetAll()
        {
            return collection.AsQueryable().ToList();
        }

        public List<Order> GetAllByExternalUserId(string id)
        {
            return collection.Find(x => x.ExternalUser != null && x.ExternalUser.Id == id).ToList();
        }

        public Order GetById(Guid id)
        {
            return collection.Find(x => x.Id == id).FirstOrDefault();
        }

        public void Insert(Order order)
        {
            collection.InsertOne(order);
        }

        public void Update(Order order)
        {
            collection.ReplaceOne(x => x.Id == order.Id, order);
        }
    }
}
