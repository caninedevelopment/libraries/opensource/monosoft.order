namespace Monosoft.Order.Persistence.PostgreSQL
{
    using Microsoft.Extensions.DependencyInjection;
    public static class DependencyInjection
    {
        public static IServiceCollection AddPostgreSQLPersistenceProvider(this IServiceCollection services)
        {
            return services;
        }
    }
}