﻿namespace Monosoft.Order.Domain.Interfaces
{
    using Domain.Entities.Order;
    using System;
    using System.Collections.Generic;

    public interface IOrderRepository
    {
        Order GetById(Guid id);
        List<Order> GetAll();
        List<Order> GetAllByExternalUserId(string id);
        void Insert(Order order);
        void Update(Order order);
        void Delete(Guid id);
    }
}
