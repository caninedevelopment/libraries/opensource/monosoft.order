﻿namespace Monosoft.Order.Domain.Entities.Order
{
    public class Address
    {
        public string Fullname { get; set; }

        public string Phone { get; set; }

        public string Company { get; set; }

        public string AdressLine1 { get; set; }

        public string AdressLine2 { get; set; }

        public string Zip { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}
