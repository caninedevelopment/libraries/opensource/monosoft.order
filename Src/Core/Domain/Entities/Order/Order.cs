﻿namespace Monosoft.Order.Domain.Entities.Order
{
    using Monosoft.Common;
    using System;
    using System.Collections.Generic;

    public class Order
    {
        public Guid Id { get; set; }

        public ExternalReference ExternalUser { get; set; }

        public TransactionStatusCode TransactionStatusCode { get; set; }

        public string TransactionStatusMessage { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? PaymentDate { get; set; }

        public DateTime? ShippedDate { get; set; } // Todo: Should this be in the Orderline class to enable different shipping dates?

        public Address BillingAddress { get; set; }

        public Address ShippingAddress { get; set; }

        public List<OrderLine> Orderlines { get; set; }
    }
}
