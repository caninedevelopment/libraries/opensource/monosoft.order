﻿namespace Monosoft.Order.Domain.Entities.Order
{
    using Monosoft.Common;

    public class OrderLine
    {
        /// <summary>
        /// Gets or sets ExternalProduct. never changing Id NOT SKU.
        /// </summary>
        public ExternalReference ExternalProduct { get; set; }

        public decimal UnitPriceExVat { get; set; }

        public int Quantity { get; set; }

        public int Vat { get; set; }

        public string Variation { get; set; }

        public OrderLine() { }

        public OrderLine(ExternalReference externalRef, decimal unitPriceExVat, int quantity, int vat, string variation)
        {
            this.ExternalProduct = externalRef;
            this.UnitPriceExVat = unitPriceExVat;
            this.Quantity = quantity;
            this.Vat = vat;
            this.Variation = variation;
        }
    }
}
