﻿namespace Monosoft.Order.Application.Resources.Order.Common
{
    using Domain.Entities.Order;
    using Monosoft.Common;

    public class OrderLineDTO
    {
        public ExternalReference ExternalProduct { get; set; }

        public decimal UnitPriceExVat { get; set; }

        public int Quantity { get; set; }

        public int Vat { get; set; }

        public string Variation { get; set; }

        public OrderLineDTO() { }

        public OrderLineDTO(OrderLine orderline)
        {
            this.ExternalProduct = orderline.ExternalProduct;
            this.Quantity = orderline.Quantity;
            this.UnitPriceExVat = orderline.UnitPriceExVat;
            this.Variation = orderline.Variation;
            this.Vat = orderline.Vat;
        }
    }
}
