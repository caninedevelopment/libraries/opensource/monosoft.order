﻿namespace Monosoft.Order.Application.Resources.Order.Common
{
    using Domain.Entities.Order;
    using Monosoft.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class OrderDTO
    {
        public Guid Id { get; set; }

        public ExternalReference ExternalUser { get; set; }

        public TransactionStatusCode TransactionStatusCode { get; set; }

        public string TransactionStatusMessage { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? PaymentDate { get; set; }

        public DateTime? ShippedDate { get; set; }

        public AddressDTO BillingAddress { get; set; }

        public AddressDTO ShippingAddress { get; set; }

        public List<OrderLineDTO> Orderlines { get; set; }

        public OrderDTO() { }

        public OrderDTO(Order order)
        {
            this.BillingAddress = new AddressDTO(order.BillingAddress);
            this.CreatedDate = order.CreatedDate;
            this.ExternalUser = order.ExternalUser;
            this.Id = order.Id;
            this.Orderlines = order.Orderlines.Select(orderline => new OrderLineDTO(orderline)).ToList();
            this.PaymentDate = order.PaymentDate;
            this.ShippedDate = order.ShippedDate;
            this.ShippingAddress = new AddressDTO(order.ShippingAddress);
            this.TransactionStatusCode = order.TransactionStatusCode;
        }
    }
}
