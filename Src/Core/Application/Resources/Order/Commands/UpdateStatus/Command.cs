namespace Monosoft.Order.Application.Resources.Order.Commands.UpdateStatus
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Command : IProcedure<Request>
    {
        private readonly IOrderRepository orderRepo;

        public Command(
            IOrderRepository orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        public void Execute(Request input)
        {
            var order = orderRepo.GetById(input.Id);

            if (order == null)
            {
                throw new ElementDoesNotExistException("No order with that id", input.Id.ToString());
            }

            order.TransactionStatusCode = input.TransactionStatusCode;

            orderRepo.Update(order);
        }
    }
}