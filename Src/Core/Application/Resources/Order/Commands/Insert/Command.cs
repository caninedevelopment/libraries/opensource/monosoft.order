namespace Monosoft.Order.Application.Resources.Order.Commands.Insert
{
    using Domain.Entities.Order;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using System.Linq;

    public class Command : IProcedure<Request>
    {
        private readonly IOrderRepository orderRepo;

        public Command(
            IOrderRepository orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        public void Execute(Request input)
        {
            var oldOrder = orderRepo.GetById(input.Id);

            if(oldOrder != null)
            {
                throw new ElementAlreadyExistsException("Order already exist", input.Id.ToString());
            }

            Address shippingAddress = input.ShippingAddress != null ? input.ShippingAddress.GetAddress() : null;
            Address billingAddress = input.BillingAddress != null ? input.BillingAddress.GetAddress() : null;

            var order = new Order()
            {
                Id = input.Id,
                ExternalUser = input.ExternalUser,
                TransactionStatusCode = TransactionStatusCode.Initializing,
                CreatedDate = DateTime.UtcNow,
                PaymentDate = input.PaymentDate,
                ShippedDate = input.ShippedDate,
                BillingAddress = billingAddress != null ? billingAddress : shippingAddress,
                ShippingAddress = shippingAddress != null ? shippingAddress : billingAddress,
                Orderlines = input.OrderLines.Select(orderline => new OrderLine(orderline.ExternalProduct, orderline.UnitPriceExVat, orderline.Quantity, orderline.Vat, orderline.Variation)).ToList()
            };

            orderRepo.Insert(order);
        }
    }
}