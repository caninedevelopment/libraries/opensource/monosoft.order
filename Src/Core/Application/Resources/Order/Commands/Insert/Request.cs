namespace Monosoft.Order.Application.Resources.Order.Commands.Insert
{
    using Application.Resources.Order.Common;
    using Monosoft.Common;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }

        public ExternalReference ExternalUser { get; set; }

        public DateTime PaymentDate { get; set; }

        public DateTime ShippedDate { get; set; }

        public AddressDTO BillingAddress { get; set; }

        public AddressDTO ShippingAddress { get; set; }

        public List<OrderLineDTO> OrderLines { get; set; }

        public void Validate()
        {
            if (Id == Guid.Empty)
            {
                throw new ValidationException(nameof(Id));
            }

            if (OrderLines == null || OrderLines.Any() == false)
            {
                throw new ValidationException(nameof(OrderLines));
            }
        }
    }
}