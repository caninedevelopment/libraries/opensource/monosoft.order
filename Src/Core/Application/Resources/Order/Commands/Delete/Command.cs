namespace Monosoft.Order.Application.Resources.Order.Commands.Delete
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly IOrderRepository orderRepo;

        public Command(
            IOrderRepository orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        public void Execute(Request input)
        {
            orderRepo.Delete(input.Id);
        }
    }
}