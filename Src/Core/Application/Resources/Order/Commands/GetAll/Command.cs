namespace Monosoft.Order.Application.Resources.Order.Commands.GetAll
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IFunction<Response>
    {
        private readonly IOrderRepository orderRepo;

        public Command(
            IOrderRepository orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        public Response Execute()
        {
            var orders = orderRepo.GetAll();

            return new Response(orders);
        }
    }
}