namespace Monosoft.Order.Application.Resources.Order.Commands.Update
{
    using Domain.Entities.Order;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System.Linq;

    public class Command : IProcedure<Request>
    {
        private readonly IOrderRepository orderRepo;

        public Command(
            IOrderRepository orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        public void Execute(Request input)
        {
            var oldOrder = orderRepo.GetById(input.Id);

            if (oldOrder == null)
            {
                throw new ElementDoesNotExistException("Order doesn't exist", input.Id.ToString());
            }

            Address shippingAddress = input.ShippingAddress != null ? input.ShippingAddress.GetAddress() : null;
            Address billingAddress = input.BillingAddress != null ? input.BillingAddress.GetAddress() : null;

            oldOrder.ExternalUser = input.ExternalUser;
            oldOrder.TransactionStatusCode = input.TransactionStatusCode;
            oldOrder.PaymentDate = input.PaymentDate;
            oldOrder.ShippedDate = input.ShippedDate;
            oldOrder.BillingAddress = billingAddress != null ? billingAddress : shippingAddress;
            oldOrder.ShippingAddress = shippingAddress != null ? shippingAddress : billingAddress;
            oldOrder.Orderlines = input.OrderLines.Select(orderline => new OrderLine(orderline.ExternalProduct, orderline.UnitPriceExVat, orderline.Quantity, orderline.Vat, orderline.Variation)).ToList();

            orderRepo.Update(oldOrder);
        }
    }
}