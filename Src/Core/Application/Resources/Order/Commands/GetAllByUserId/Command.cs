namespace Monosoft.Order.Application.Resources.Order.Commands.GetAllByUserId
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IFunction<Request, Response>
    {
        private readonly IOrderRepository orderRepo;

        public Command(
            IOrderRepository orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        public Response Execute(Request input)
        {
            var orders = orderRepo.GetAllByExternalUserId(input.ExternalId);

            return new Response(orders);
        }
    }
}