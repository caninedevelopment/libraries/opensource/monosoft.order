namespace Monosoft.Order.Application.Resources.Order.Commands.GetAllByUserId
{
    using Application.Resources.Order.Common;
    using Monosoft.Common.Command.Interfaces;
    using Domain.Entities.Order;
    using System.Collections.Generic;
    using System.Linq;

    public class Response : IDtoResponse
    {
        public List<OrderDTO> Orders { get; set; }

        public Response(List<Order> orders)
        {
            this.Orders = orders.Select(x => new OrderDTO(x)).ToList();
        }
    }
}