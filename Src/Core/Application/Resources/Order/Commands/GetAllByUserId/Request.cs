namespace Monosoft.Order.Application.Resources.Order.Commands.GetAllByUserId
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public string ExternalId { get; set; }
        public void Validate()
        {
            if (string.IsNullOrEmpty(ExternalId))
            {
                throw new ValidationException(nameof(ExternalId));
            }
        }
    }
}