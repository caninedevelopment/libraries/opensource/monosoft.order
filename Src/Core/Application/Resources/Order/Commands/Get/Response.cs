namespace Monosoft.Order.Application.Resources.Order.Commands.Get
{
    using Application.Resources.Order.Common;
    using Domain.Entities.Order;
    using Monosoft.Common.Command.Interfaces;

    public class Response : OrderDTO, IDtoResponse
    {

        public Response(Order order)
            : base(order)
        {
        }
    }
}