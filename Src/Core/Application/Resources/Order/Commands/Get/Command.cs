namespace Monosoft.Order.Application.Resources.Order.Commands.Get
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IFunction<Request, Response>
    {
        private readonly IOrderRepository orderRepo;

        public Command(
            IOrderRepository orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        public Response Execute(Request input)
        {
            var order = orderRepo.GetById(input.Id);

            return new Response(order);
        }
    }
}