namespace Monosoft.Order.Application.Resources.Order.Commands.Get
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }
        public void Validate()
        {
            if (Id == Guid.Empty)
            {
                throw new ValidationException(nameof(Id));
            }
        }
    }
}